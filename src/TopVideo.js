import React, { Component } from 'react';
import './App.css';

class TopVideo extends Component {
  render () {
    var now = new Date();
    var makeTime = new Date(this.props.date);
    var timeCount = Math.floor(Math.floor(now - makeTime)/(1000*60*60));
    if (timeCount > 24) {
      timeCount = Math.floor(timeCount/24);
      if(timeCount > 31){
        timeCount = Math.floor(timeCount/30);
        if (timeCount > 12) {
          timeCount = Math.floor(timeCount/12);
          timeCount = timeCount + '년 전';
        } else {
          timeCount = timeCount + '개월 전';
        }
      } else {
        timeCount = timeCount + '일 전';
      }
    } else {
      timeCount = timeCount + '시간 전';
    }
    var viewCount = this.props.viewCount;
    if (viewCount > 1000) {
      viewCount = Math.floor(viewCount/100)/10;
      if (viewCount > 10) {
        viewCount = Math.floor(viewCount)/10;
        if (viewCount > 10) {
          viewCount = Math.floor(viewCount);
          viewCount = viewCount + '만회';
        } else {
          viewCount = viewCount + '만회';
        }
      } else {
        viewCount = viewCount + '천회';
      }
    } else {
      viewCount = viewCount + '회';
    }
    let duration = this.props.time.replace('h',':').replace('m',':').replace('s','');
    let renderDuration = duration.split(':').map(num => {
      if (num.length < 2) {
        return new Array(3 - num.length).join('0') + num;
      } else {
        return num;
      }
    }).join(':');
      return (
        <div className='video-img'>
          <p className='top-video-language'>{this.props.language === 'ko' ? 'Kor' : 'Eng'}</p>
          <p className='top-video-viewable'>{this.props.viewable === 'public' ? '공개' : '비공개'}</p>
          <p className='top-video-time'>{renderDuration}</p>
          <img
            onClick={this.props.showingModal.bind(this,this.props.urlId,
            this.props.description,
            this.props.title,
            this.props.userName,
            this.props.viewCount,
            this.props.language,
            this.props.viewable
          )}
          src={this.props.url}/>
          <p className='top-video-name'>{this.props.title.slice(0, 70)}</p>
          <p className='top-user-name'>{this.props.userName}</p>
          <p className='top-view-count'>조회수 {viewCount} · {timeCount}</p>
        </div>
      );
    }
  }

  export default TopVideo;
