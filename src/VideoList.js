import React, { Component } from 'react';
import './App.css';
import TopVideo from './TopVideo'
import BtnControl from './BtnControl'

class VideoList extends Component {
    render () {
      return (
         <>
          <div className='videoList'>
              {this.props.videoList.map((videoArray, i) => {
                return (<TopVideo id={this.props.id}
                  url={videoArray.url}
                  urlId={videoArray.urlId}
                  title={videoArray.title}
                  time={videoArray.time}
                  userName={videoArray.userName}
                  viewCount={videoArray.viewCount}
                  date={videoArray.date}
                  language={videoArray.language}
                  viewable={videoArray.viewable}
                  description={videoArray.description}
                  showingModal={this.props.showingModal}
                  key={i}/>);
                  })}
          </div>
          <BtnControl id={this.props.id}
          nextPage={this.props.nextPage}
          clickEvent={this.props.pageControler.bind(this)}/>
        </>
      );
    }
  }

export default VideoList;
