import React, { Component } from 'react';
import './App.css';
import home from './img/home.png'
import popular from './img/Popular.png'
import twitch from './img/twitch.png'
import streaming from './img/streaming.png'
import SideTopGame from './SideTopGame'

class Side extends Component {
    render() {
        return (
            <div className='side-menu'>
            <div className='first-side'>
              <a href='https://www.twitch.tv/' className='side-list'>
                <div><img className='side-list-img' src={home}/></div>
                <div className='side-list-text'>홈</div>
              </a>
              <div onClick={this.props.clickPopular} className='side-list'>
                <div><img className='side-list-img' src={popular}/></div>
                <div className='side-list-text'>인기</div>
              </div>
              <a href='https://www.twitch.tv/lck_korea' className='side-list'>
                <div><img className='side-list-img' src={twitch}/></div>
                <div className='side-list-text'>구독</div>
              </a>
              <a href='https://www.twitch.tv/' className='side-list'>
                <div><img className='side-list-img' src={streaming}/></div>
                <div className='side-list-text'>스트리밍</div>
              </a>
             </div>
             <div className='second-side'>
              <div className='second-side-title'>인기 게임</div>
              {this.props.topList.map((urlArray, i) => {
                  return (<SideTopGame
                    name={urlArray.name}
                    id={urlArray.id}
                    topGameImg={urlArray.topGameImg}
                    gameChanger={this.props.gameChanger}
                    key={i}/>);
                  })}
             </div>
          </div>
        );
    }
}

export default Side;
