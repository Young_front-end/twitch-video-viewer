import React, { Component } from 'react';
import './App.css';
import Fortnite from './img/fortnite.png'
import lol from './img/lol.png'
import dota from './img/dota.png'
import pubg from './img/pubg.png'
import heart from './img/Heart.png'
import Overwatch from './img/overwatch.png'
import gta5 from './img/gta5.png'
import cs from './img/cs.png'
import jc from './img/jc.png'
import wc from './img/wc.png'
import tt from './img/tt.png'
import mc from './img/mc.png'
import dd from './img/dd.png'
import cod from './img/cod.png'
import apex from './img/apex.png' 

const imgMap = {
    Fortnite : Fortnite,
    'League of Legends' : lol,
    Overwatch : Overwatch,
    'Grand Theft Auto V' : gta5,
    'Counter-Strike: Global Offensive' : cs,
    'Just Chatting' : jc,
    'World of Warcraft' : wc,
    'Teamfight Tactics' : tt,
    'Apex Legends' : apex,
    Hearthstone : heart,
    Minecraft : mc,
    'Dota 2' : dota,
    'Dungeons & Dragons' : dd,
    "PLAYERUNKNOWN'S BATTLEGROUNDS" : pubg
}

class SideTopGame extends Component {
    render () {
        return (
            <div onClick={() => this.props.gameChanger(this.props.id, this.props.name, this.props.topGameImg)} className='game-list'>
                <div><img className='game-list-img' src={imgMap[this.props.name]}/></div>
                <div className='game-list-text'>{this.props.name.slice(0, 16)}</div>
            </div>
        );
    }
}

export default SideTopGame;
