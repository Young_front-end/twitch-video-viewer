import React, { Component } from 'react';
import './App.css';

class TopGame extends Component {
    render () {
      return (
        <div className='game-img'>
          <img onClick={() => this.props.clickChange(this.props.id, this.props.name, this.props.topGameImg)} src={this.props.url}/>
          <p className='top-game-name'>{this.props.name}</p>
        </div>
      );
    }
  }

export default TopGame;
