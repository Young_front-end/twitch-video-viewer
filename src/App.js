import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import TopGame from './TopGame'
import VideoList from './VideoList'
import Modal from './Modal'
import Side from './Side'
import SmallSide from './SmallSide'
import sideBar from './img/side-bar.png'
import twitchTitle from './img/twitch-title.png'
import twitch from './img/twitch.png'

class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      topList : [],
      videoList : [],
      currentGame : null,
      pageControl : null,
      modalShowig : false,
      modalId : null,
      sideBarEvent : false,
      desc : null,
      modalTitle : null,
      modalUserName : null,
      modalViewCount : null,
      modalLanguage : null,
      modalViewable : null,
      topGameImg : null
    }
  }
  inputState(arr, res) {
    var dataLength;
    if (res.data.data.length > 20) {
      dataLength = 20;
    } else {
      dataLength = res.data.data.length;
    }
    for (var i = 0; i < dataLength; i++) {
      arr[i] = {};
      arr[i].url = res.data.data[i].thumbnail_url.replace('%{width}x%{height}','330x170');
      arr[i].title = res.data.data[i].title;
      arr[i].time = res.data.data[i].duration;
      arr[i].userName = res.data.data[i].user_name;
      arr[i].viewCount = res.data.data[i].view_count;
      arr[i].urlId = res.data.data[i].id;
      arr[i].date = res.data.data[i].created_at;
      arr[i].language = res.data.data[i].language;
      arr[i].viewable = res.data.data[i].viewable;
      arr[i].description = res.data.data[i].description;
    }
  }
  componentDidMount() {
    this._getList();
  }
  _getList(){
    axios.get('https://api.twitch.tv/helix/games/top', {
      headers: {
        'Client-ID':'jft2f7dybmikpbufem1uuw5vyeca99'
      }
    }).then(response => {
      var urlArray = [];
      for(var i = 0; i < 10; i++) {
        urlArray[i] = {};
        urlArray[i].url = response.data.data[i].box_art_url.replace('{width}x{height}','130x190');
        urlArray[i].name = response.data.data[i].name;
        urlArray[i].id = response.data.data[i].id;
        urlArray[i].topGameImg = response.data.data[i].box_art_url.replace('{width}x{height}','720x950');
      }
      this.setState({
        topList : urlArray,
        currentGame : urlArray[0].name,
        currentGameId : urlArray[0].id,
        topGameImg : urlArray[0].topGameImg
      })
      this._getVideoList(this.state.currentGameId);
    })
  }
  _getVideoList(currentGameId){
    axios.get(`https://api.twitch.tv/helix/videos?game_id=${currentGameId}&language=ko&sort=views`, {
          headers: {
            'Client-ID':'jft2f7dybmikpbufem1uuw5vyeca99'
          }
        }).then(res => {
          var videoArray = [];
          this.inputState(videoArray,res);
          this.setState({
            videoList : videoArray,
            pageControl : res.data.pagination,
            nextPage : res.data.pagination.cursor,
            modalId : videoArray[0].urlId
          })
        })
  }

  pageControler(id, dir, pageInfo) {
    axios.get(`https://api.twitch.tv/helix/videos?game_id=${id}&language=ko&sort=views&${dir}=${pageInfo}`, {
        headers: {
            'Client-ID':'jft2f7dybmikpbufem1uuw5vyeca99'
        }
    }).then(res => {
      var videoArrayNext = [];
      this.inputState(videoArrayNext,res);
      this.setState({
        videoList : videoArrayNext,
        nextPage : res.data.pagination.cursor
      })
    })
}
  
gameChanger(id,game,url) {
  this.setState({
    currentGameId : id,
    currentGame : game,
    topGameImg : url
  })
  this._getVideoList(id);
}
clickPopular(){
  this.setState({
    currentGameId : this.state.topList[0].id,
    currentGame : this.state.topList[0].name
  })
  this._getVideoList(this.state.topList[0].id);
}

showingModal(id, desc, title, userName, viewCount, language, viewable) {
  this.setState({
    modalShowig : !this.state.modalShowig,
    modalId : id,
    desc : desc,
    modalTitle : title,
    modalUserName : userName,
    modalViewCount : viewCount,
    modalLanguage : language,
    modalViewable : viewable
  })
}

sideBarClick(){
  this.setState({
    sideBarEvent : !this.state.sideBarEvent
  })
}
videoUrl = {
  Fortnite : 'https://www.youtube.com/embed/2gUtfBmw86Y?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'League of Legends' : 'https://www.youtube.com/embed/fB8TyLTD7EE??mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  Overwatch : 'https://www.youtube.com/embed/oJ09xdxzIJQ?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Grand Theft Auto V' : 'https://www.youtube.com/embed/QkkoHAzjnUs?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Counter-Strike: Global Offensive' : 'https://www.youtube.com/embed/edYCtaNueQY?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Just Chatting' : 'https://www.youtube.com/embed/eMphMz5yUGg?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'World of Warcraft' : 'https://www.youtube.com/embed/KPt2BRe5Lzg?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Teamfight Tactics' : 'https://www.youtube.com/embed/liNLLx874g4?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Apex Legends' : 'https://www.youtube.com/embed/oQtHENM_GZU?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  Hearthstone : 'https://www.youtube.com/embed/lMhpHgqIQdQ?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  Minecraft : 'https://www.youtube.com/embed/cPJUBQd-PNM?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Dota 2' : 'https://www.youtube.com/embed/-cSFPIwMEq4?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Dungeons & Dragons' : 'https://www.youtube.com/embed/Eo7dce8rDUY?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  "PLAYERUNKNOWN'S BATTLEGROUNDS" : 'https://www.youtube.com/embed/hfjazBN0DwA?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0',
  'Call of Duty: Black Ops 4' : 'https://www.youtube.com/embed/6kqe2ICmTxc?mute=1&loop=1&autoplay=1&rel=0&controls=0&showinfo=0'
}

  render() {
    var mainContentPadding = 'main-content';
    if (this.state.sideBarEvent) {
      mainContentPadding = 'main-content-small';
    } else {
      mainContentPadding = 'main-content';
    }
    var style = this.state.topGameImg;
    var topMainVideoUrl;
    for (var prop in this.videoUrl) {
      if(prop === this.state.currentGame) {
        topMainVideoUrl = this.videoUrl[prop];
      }
    }
    return (
      <div className='App'>
        <div className='header'>
          <div className='side-bar-wraper'>
            <img onClick={this.sideBarClick.bind(this)} className='side-bar' src={sideBar}/>
            <img className='side-logo' src={twitch}/>
            <img className='twitch-title' src={twitchTitle}/>
          </div>
          <h1 className='header-title'>Twitch Top Games</h1>
        </div>
        <div className='main'>
          {this.state.sideBarEvent ?
          <SmallSide clickPopular={this.clickPopular.bind(this)}/> :
          <Side clickPopular={this.clickPopular.bind(this)}
          topList={this.state.topList}
          gameChanger={this.gameChanger.bind(this)}
          />}
          <div className={mainContentPadding}>
            <div className='top-list'>
              <div className='top-list-title'>Real Time Popular Games</div>
              <div className='game-img-wraper'>
                {this.state.topList.map((urlArray, i) => {
                  return (<TopGame
                    currentGame={this.state.currentGame}
                    url={urlArray.url}
                    topGameImg={urlArray.topGameImg}
                    name={urlArray.name}
                    id={urlArray.id}
                    clickChange={this.gameChanger.bind(this)}
                    key={i}/>);
                  })}
              </div>
            </div>
            <div className='top-game-title'>
              <div className='top-game-text'>
                <p className='top-game-text-title'>{this.state.currentGame}</p>
                <img className='top-game-img' src={style}/>
                <p className='top-game-info'><span className='top-game-info-title'>{this.state.currentGame}</span>, 지금 시청하세요</p>
                <p className='top-game-info-detail'>스트리머들의 방송에 참여해주세요!<br/>당신의 참여가 더욱 풍족한 콘텐츠를 만들어냅니다.</p>
                <div onClick={this.state.videoList.length !==0 ? this.showingModal.bind(this,this.state.videoList[0].urlId,
                this.state.videoList[0].description,
                this.state.videoList[0].title,
                this.state.videoList[0].userName,
                this.state.videoList[0].viewCount,
                this.state.videoList[0].language,
                this.state.videoList[0].viewable) : this.showingModal.bind(this)} className='top-game-video-play'>인기방송<br/>시청하기</div>
                <div href='https://www.twitch.tv/' className='top-game-stream'>스트리밍<br/>보러가기</div>
              </div>
              <div className='top-game-main-video'>
                <iframe width="1280" height="720" src={topMainVideoUrl} frameborder="0" allow="autoplay; encrypted-media" allowfullscreen></iframe>
              </div>
            </div>
            <div className='games-video-wrap'>
              {this.state.topList.length === 0 ? <div></div> : <VideoList id={this.state.currentGameId}
              videoList={this.state.videoList}
              currentGame={this.state.currentGame}
              nextPage={this.state.nextPage}
              showingModal={this.showingModal.bind(this)}
              pageControler={this.pageControler.bind(this)}
              key={0}/>}
            </div>
           </div>
          </div>
        {this.state.modalShowig && <Modal showingModal={this.showingModal.bind(this)}
        urlId={this.state.modalId}
        desc={this.state.desc}
        title={this.state.modalTitle}
        userName={this.state.modalUserName}
        viewCount={this.state.modalViewCount}
        language={this.state.modalLanguage}
        viewable={this.state.modalViewable}/>}
      </div>
    );
  }
}

export default App;
