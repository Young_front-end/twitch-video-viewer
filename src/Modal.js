import React, { Component } from 'react';
import './App.css';

class Modal extends Component {
    render() {
        console.log('modal : ',this.props);
        return (
            <div onClick={this.props.showingModal} className='modal'>
                <div className='modal-video-wrapper'>
                    <iframe className='video-modal' src={`https://player.twitch.tv/?video=v${this.props.urlId}&autoplay=true`}
                    height='480'
                    width='720'
                    frameborder='0'
                    scrolling='no'
                    allowfullscreen='true'>
                    </iframe>
                    <div className='modal-desc'>
                        <p className='modal-desc-title'>비디오 상세설명 :</p>
                        <p className='modal-desc-desc'>&#32;{this.props.desc === '' ? ' No description' : this.props.desc}</p>
                        <div className='modal-desc-sub-wrapper'>
                            <p className='modal-desc-sub'>비디오 타이틀 : {this.props.title}</p>
                            <p className='modal-desc-sub'>비디오 제작자 : {this.props.userName}</p>
                            <p className='modal-desc-sub'>조회수 : {this.props.viewCount}</p>
                            <p className='modal-desc-sub'>언어 : {this.props.language}</p>
                            <p className='modal-desc-sub'>공개 여부 : {this.props.viewable}</p>
                        </div>
                    </div>
                </div>
            </div>            
        );
    }
}

export default Modal;
