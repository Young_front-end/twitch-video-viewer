import React, { Component } from 'react';
import './App.css';

class BtnControl extends Component {
  render () {
    return (
      <div className="page-control">
        <div className="btn-wraper">
          <p onClick={this.props.clickEvent.bind(this,this.props.id ,'before',this.props.nextPage)} className='page-btn-left'>&#60;</p>
          <p onClick={this.props.clickEvent.bind(this,this.props.id ,'after',this.props.nextPage)} className='page-btn-right'>&#62;</p>
        </div>
      </div>
      );
    }
  }

export default BtnControl;
