import React, { Component } from 'react';
import './App.css';
import home from './img/home.png'
import popular from './img/Popular.png'
import twitch from './img/twitch.png'
import streaming from './img/streaming.png'

class SmallSide extends Component {
    render () {
        return (
            <div className='side-small-menu'>
                <a href='https://www.twitch.tv/' className='small-home'>
                    <img className='small-home-img' src={home}/>
                    <div className='small-home-text'>홈</div>
                </a>
                <div onClick={this.props.clickPopular} className='small-popular'>
                    <img className='small-popular-img' src={popular}/>
                    <div className='small-popular-text'>인기</div>
                </div>
                <a href='https://www.twitch.tv/lck_korea' className='small-twitch'>
                    <img className='small-twitch-img' src={twitch}/>
                    <div className='small-twitch-text'>구독</div>
                </a>
                <a href='https://www.twitch.tv/' className='small-streaming'>
                    <img className='small-streaming-img' src={streaming}/>
                    <div className='small-streaming-text'>스트리밍</div>
                </a>
            </div>
        );
    }
}

export default SmallSide;
